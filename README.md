Checks for Lambda functions with high error rates that may result in high cost. Lambda charges based on the number of requests and aggregate execution time for your function. Function errors may cause retries that incur additional charges.
Note: Results for this check are automatically refreshed several times daily, and refresh requests are not allowed. It might take a few hours for changes to appear.

Alert Criteria
Yellow: Functions where > 10% of invocations end in error on any given day within the last 7 days.


Recommended Action
Consider the following guidelines to reduce errors. Function errors include errors returned by the function's code and errors returned by the function's runtime. To help you troubleshoot Lambda errors, Lambda integrates with services like Amazon CloudWatch and AWS X-Ray. You can use a combination of logs, metrics, alarms, and X-ray tracing to quickly detect and identify issues in your function code, API, or other resources that support your application. For more information, see Monitoring and troubleshooting Lambda applications. For more information on handling errors with specific runtimes, see Error handling and automatic retries in AWS Lambda. For additional troubleshooting, see Troubleshooting issues in Lambda.You can also choose from an ecosystem of monitoring and observability tools provided by AWS Lambda partners. For additional information about Partners, see AWS Lambda Partners.
